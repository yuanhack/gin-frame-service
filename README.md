[TOC]

### 目录结构
根据个人开发习惯，go项目组织结构可能因人而异  
以下是个人感觉最适合的最优配置
```bash
go
├── gothird/                 # GOPATH 全局第三方共享库
├── goshare/                 # GOPATH 全局共享库
├── project1/                # GOPATH 项目1（模块以及共享库）
└── project2/                # GOPATH 项目2（模块以及共享库）
```
某项目的GOPATH应包含 gothird 第三方库，goshare 全局共享库(如有) 和project1项目路径   
比如：`GOPATH=/go/gothird:/go/goshare:/go/project1 ...`

#### 全局第三方共享库
单独路径存放，以方便管理
```bash
gothird/
├── bin/
├── pkg/
└── src/
     ├─ github.com/...
     └─ other/...
```
如果 git 设置 gobase 为第三方库的根目录 .gitignore 中 可能要设置忽略 pkb/ bin/ 等目录

#### 项目和本地共享库
项目路径与第三方库以及全局共享库分离，以方便管理(复制和备份或直接拷贝给他人等)
```bash
project1/
├── bin/
├── pkg/
└── src/
     ├─ go-share/            # 本地共享库
     ├─ gin-frame-service/   # 示例项目模块
     ├─ other1/...           # 其他项目模块
     └─ other2/...           # 其他项目模块
```
