package controllers

// 授权码
const (
	ENCRYPT_HEAD = "hainaV2"
)

// 牛币变化消息推送
const (
	MESSAGE_NIUCOIN_TITLE             = "牛币变动通知"
	MESSAGE_NIUCOIN_PERSONAL_REGISTER = "恭喜您成功注册海纳智投，%d牛币已到账。牛币可在购买产品时使用，点击查看详情！"
	MESSAGE_NIUCOIN_PERSONAL_BUY      = "购买产品奖励%d牛币已到账，牛币可在购买产品时使用，点击查看详情！"
	MESSAGE_NIUCOIN_FRIEND_REGISTER   = "您邀请的好友%s注册成功，您已获得%d牛币奖励，点击查看详情！"
	MESSAGE_NIUCOIN_FRIEND_BUY        = "您邀请的好友%s成功购买产品，您已获得%d牛币奖励。点击查看详情！"
	MESSAGE_NIUCOIN_USE               = "您于%s购买%s使用%d牛币，剩余牛币%d个。点击查看详情！"
)
