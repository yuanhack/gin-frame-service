package config

import (
	"os"
	"os/exec"
	"path/filepath"

	"go-share/lib"
)

var config *AppConfig

type AppSettings struct {
	AllowOrigin string     `xml:"allowOrigin"`
	EncryFactor string     `xml:"encryFactor"`
	Environment string     `xml:"environment"`
	Listen      string     `xml:"listen"`
	Projects    []Projects `xml:"projects"`
}

type Projects struct {
	AppId      string `xml:"appId"`
	ConfigFile string `xml:"configFile"`
}

type AccessKeys struct {
	ID     string `xml:"id"`
	Secret string `xml:"secret"`
	AESKey string `xml:"aesKey"`
}

// Database
type Database struct {
	DriverName string `xml:"driverName"`
	DataSource string `xml:"dataSource"`
}

// MongoDB
type MongoStore struct {
	Source string `xml:"source"`
}

// Redis
type RedisStore struct {
	Addr    string `xml:"addr"`
	Auth    string `xml:"auth"`
	Db      string `xml:"db"`
	Timeout int    `xml:"timeout"`
}

// Email
type EmailSetting struct {
	Addr     string `xml:"addr"`
	Password string `xml:"password"`
	Server   string `xml:"server"`
	Port     string `xml:"port"`
}

type SessionSetting struct {
	On           bool   `xml:"on"`
	ProviderName string `xml:"providerName"`
	Config       string `xml:"config"`
}
type MnsSetting struct {
	Url             string    `xml:"url"`
	AccessKeyId     string    `xml:"accessKeyId"`
	AccessKeySecret string    `xml:"accessKeySecret"`
	Queues          QueueName `xml:"queues"`
}
type QueueName struct {
	SmartCall string `xml:"exchangeSmartCall"`
}

type LogServer struct {
	On   bool   `xml:"on"`
	Addr string `xml:"addr"`
	Port string `xml:"port"`
}

type AppConfig struct {
	AccessKeys AccessKeys     `xml:"accessKeys"`
	APNS       APNSSetting    `xml:"iosAPNS"`
	Cors       CorsSetting    `xml:"cors"`
	Db         Database       `xml:"database"`
	Email      EmailSetting   `xml:"emailSetting"`
	Mns        MnsSetting     `xml:"mns"`
	Mongo      MongoStore     `xml:"mongoStore"`
	Jpush      JpushSetting   `xml:"jpush"`
	Redis      RedisStore     `xml:"redisStore"`
	Serve      ListenAndServe `xml:"listenAndServe"`
	Session    SessionSetting `xml:"session"`
	Settings   AppSettings    `xml:"appSettings"`
	Log        LogServer      `xml:"logServer"`
	Payment    PaymentSetting `xml:"payment"`
	Xml        XmlDomain      `xml:"xmlDomain"`
	Crime      CrimeSetting   `xml:"crime"`
}

type CrimeSetting struct {
	Weblive   CrimeItem `xml:"weblive"`
	Opinion   CrimeItem `xml:"opinion"`
	CourseBuy CrimeItem `xml:"courseBuy"`
}

type CrimeItem struct {
	BaseMin  int `xml:"baseMin"`
	BaseMax  int `xml:"baseMax"`
	TimesMin int `xml:"timesMin"`
	TimesMax int `xml:"timesMax"`
}

type APNSSetting struct {
	Production bool `xml:"production"`
}

type XmlDomain struct {
	Domain string `xml:"domain"`
}
type JpushSetting struct {
	AppKey       string `xml:"appKey"`
	MasterSecret string `xml:"masterSecret"`
}

type PaymentSetting struct {
	AliyPay   AliyPaySetting   `xml:"aliypay"`
	WechatPay WechatPaySetting `xml:"wechat"`
}
type AliyPaySetting struct {
	AppID     string `xml:"appid"`
	Partner   string `xml:"partner"`
	SellerID  string `xml:"seller"`
	Key       string `xml:"key"`
	NotifyUrl string `xml:"notify_url"`
	ReturnUrl string `xml:"return_url"`
}
type WechatPaySetting struct {
	AppID     string `xml:"appid"`
	MchID     string `xml:"mch_id"`
	Key       string `xml:"key"`
	NotifyUrl string `xml:"notify_url"`
}

type Login struct {
	RegisterPage string `xml:"register_page"`
	CurrencyGet  string `xml:"currency_get"`
}

type URLSetting struct {
	Reg Login `xml:"login"`
}

type SNS struct {
	AuthUrl        string `xml:"auth_url"`
	AccessTokenUrl string `xml:"access_token_url"`
	ClientId       string `xml:"client_id"`
	ClientSecret   string `xml:"client_secret"`
	RedirectUrl    string `xml:"redirect_url"`
	UserInfoUrl    string `xml:"user_info_url"`
}

type SNSSetting struct {
	Sina   SNS `xml:"sina"`
	Wechat SNS `xml:"wechat"`
	QQ     SNS `xml:"qq"`
	JD     SNS `xml:"jd"`
}

type SNSType struct {
	Mobile SNS `xml:"mobile"`
	Web    SNS `xml:"web"`
}
type SNSSetting2 struct {
	Sina   SNSType `xml:"sina"`
	Wechat SNSType `xml:"wechat"`
	QQ     SNSType `xml:"qq"`
	JD     SNSType `xml:"jd"`
}

type CorsSetting struct {
	AllowOrigin []string `xml:"allowOrigin"`
}

type ListenAndServe struct {
	Port    string `xml:"port"`
	LogPort string `xml:"logport"`
}

func Default(appID string) *AppConfig {
	if config == nil {
		var cfg AppConfig
		lib.LoadConfig(appID, &cfg)
		config = &cfg
	}
	return config
}

func Reload() {
	config = nil
}

// ------------------------------------------------------------------------

func getCurrPath() string {
	file, _ := exec.LookPath(os.Args[0])
	return filepath.Dir(file)
}
