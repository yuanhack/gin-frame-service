package models

// App Setting
//---------------------------------------------------------------------------------
const (
	APP_NAME    = "server_demo"
	APP_VERSION = "1.0.0.0"
	APP_PID     = "server_demo"
)
