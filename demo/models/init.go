package models

import (
	"gin-frame-service/demo/config"
	"go-share/logging"
	"go-share/models"
)

var Config *config.AppConfig

func init() {

	Config = config.Default(APP_PID)

	// 初始化 MySQL 配置
	err := models.Init(Config.Db.DriverName, Config.Db.DataSource)
	if err != nil {
		logging.Fatal(err)
		return
	}
}
